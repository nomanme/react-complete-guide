import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person';

class App extends Component {

  state = {
    persons: [
      { name: "Mohammad Noman", age: 25 },
      { name: "Mehe Sultana", age: 21 },
      { name: "Max Swa", age: 28 }
    ]
  }

  switchNameHandler = (newName) => {
    // console.log('was clicked!');
    // DON'T DO THIS: this.state.persons[0].name = "Mohammad Noman";
    this.setState({
      persons: [
        { name: newName, age: 25 },
        { name: "Mehe", age: 21 },
        { name: "Max", age: 30 }
      ]
    })
  }

  nameChangeHandler = (event) => {
    this.setState({
      persons: [
        { name: 'Noman', age: 25 },
        { name: event.target.value, age: 21 },
        { name: "Max", age: 33 }
      ]
    })
  }


  render() {

    const style = {
      backgroundColor: 'white',
      font: 'ingerit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer'
    }

    return (
      <div className="App">
        <h1>Hi, I'm React Developer!</h1>
        <p>This is working now</p>

        <button
          style={style}
          onClick={() => this.switchNameHandler.bind(this, 'Nayeem')}>Switch Name
        </button>

        <Person
          name={this.state.persons[0].name} age={this.state.persons[0].age}>
        </Person>
        <Person
          name={this.state.persons[1].name} age={this.state.persons[1].age} click={this.switchNameHandler.bind(this, 'noman!!')} changed={this.nameChangeHandler}>Her Hobbies: Reading Books
        </Person>
        <Person
          name={this.state.persons[2].name} age={this.state.persons[2].age}>
        </Person>

      </div>
    );
  }
  // return React.createElement('div', {className: 'App' }, React.createElement('h1', null, 'Does this work now ? '));
}

export default App;